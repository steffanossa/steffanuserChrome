# steffanuserChrome
![Alternative text](demo/standard.png "Demo")

- single bar style
- clean context menus
- sexy


## Installation
*(made using Firefox 116 on debian 12 FHD, not tested elsewhere)*

1. Go to `about:config`,
    - enter `toolkit.legacyUserProfileCustomizations.stylesheets`,
    - set it to `true`,
    - enter `layout.css.has-selector.enabled`,
    - set it to `true`;
2. Go to `about:support` and copy your profile directory
3. Clone this repo into the profile directory: `git clone https://codeberg.org/steffanossa/steffanuserChrome.git YOUR_PROFILE_DIRECTORY` or download this repo as .zip and extract the `chrome` folder into your profile directory,
4. Restart Firefox, done.

### Todos/Bugs
- ~~dark mode breaks everything~~
- personalbar disabled as of now *(might be a style decision, might be cause i haven't figured out some things yet)*
- **single-tab-windows cannot be merged together**
- **burger icon collisions**
- urlbar-breakout seems somehwat off
- dragging using alltabs-button opens context menu
- clean up redundant code